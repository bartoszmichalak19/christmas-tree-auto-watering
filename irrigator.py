from enum import Enum

from pump import Pump
from utils import *
from water_level import WaterLevel


class Tresholds(Enum):
    MIN = 0
    MAX = 100


class Irrigator:

    def __init__(self):
        config = get_config()
        print(config)
        self.pump = Pump(config['pump_pin'])
        self.water_level = WaterLevel(config['electrodes'])

    def run(self):
        if self.water_level.get() < Tresholds.MIN:
            if not self.pump.is_on:
                self.pump.on()
        elif self.water_level.get() > Tresholds.MAX:
            if self.pump.is_on:
                self.pump.off()
