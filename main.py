from irrigator import Irrigator

if __name__ == '__main__':
    christmas_tree_irrigator = Irrigator()
    christmas_tree_irrigator.run()
