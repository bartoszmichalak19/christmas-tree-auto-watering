from machine import Pin


class Pump:
    def __init__(self, pin):
        self.pump_pin = Pin(pin, Pin.OUT)
        self.pump_pin.off()
        self.is_on = False

    def on(self):
        self.pump_pin.high()
        self.is_on = True

    def off(self):
        self.pump_pin.low()
        self.is_on = False
