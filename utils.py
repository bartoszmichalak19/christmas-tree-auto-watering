import json


def get_config():
    with open("config.json") as f:
        data = json.loads(f.read())
        return data
    return None
