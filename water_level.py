from machine import Pin


# TODO: turning on reading only when needed (output pin to supply reference electrode)
class WaterLevel:
    """Class responsible for reading discrete water level"""

    def __init__(self, electrodes_config):
        self.level = 0
        for electrode in electrodes_config:
            self.electrodes.append(Pin(electrode["pin"], Pin.IN, Pin.PULL_DOWN), electrode["level_percentage"])

    def get(self):
        self.level = 0
        for electrode in self.electrodes:
            is_shorted = electrode[0].value()
            if is_shorted:
                self.level = electrode[1]
                if self._is_lower_electrodes_shorted():
                    print("Water level error: lower electrodes are not shorted")
                return self.level

    def _is_lower_electrodes_shorted(self):
        for electrode in self.electrodes:
            if electrode[1] < self.level:
                is_shorted = electrode[0].value()
                if not is_shorted:
                    return False
        return True
