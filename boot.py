import gc
import time

import network
import webrepl
from utils import *

webrepl.start()
gc.collect()

ssid = get_config()['ssid']
passwd = get_config()['passwd']
connection_timeout = get_config()['conn_timeout']

network.WLAN(network.AP_IF).active(False)
wlan = network.WLAN(network.STA_IF)
wlan.active(True)
wlan.connect(ssid, passwd)
counter = 0

# The esp8266 usually autoconnects to the last known AP.
# If you connect manually every time it wakes up, you can wear out the flash
# where the AP info is stored.
while not wlan.isconnected():
    print("waiting for autoconnect:" + str((connection_timeout / 2) - counter) + "Seconds")
    time.sleep(1)
    counter += 1
    if counter > connection_timeout:
        print("autoconnect failed")
        break

counter = 0
print("Trying to connect with: " + ssid)
while not wlan.isconnected():
    time.sleep(1)
    counter += 1
    if counter == connection_timeout:
        print('Not connected')
        break
print('Connected. \nConfig: ', wlan.ifconfig())
